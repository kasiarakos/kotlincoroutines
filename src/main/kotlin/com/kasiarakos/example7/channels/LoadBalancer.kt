package com.kasiarakos.example7.channels

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

const val numberOfWorkers = 10
const val totalWork = 10

data class Work(var x: Long = 0, var y: Long = 0, var z: Long = 0)

suspend fun worker(input: Channel<Work>, output: Channel<Work>) {

    for (w in input) {
        w.z = w.x * w.y
        delay(w.z)
        output.send(w)
    }
}

fun main() {
    val complete = Channel<Boolean>()
    run(complete)
    runBlocking {
        println(complete.receive())
        complete.close()
    }
}

fun run(complete: Channel<Boolean>) {
    val input = Channel<Work>()
    val output = Channel<Work>()

    repeat(numberOfWorkers) {
        GlobalScope.launch { worker(input, output) }
    }

    GlobalScope.launch { sendLotsOfWorks(input) }
    GlobalScope.launch { receiveLotsOfResults(output, complete) }
}

suspend fun sendLotsOfWorks(input: Channel<Work>) {
    repeat(totalWork) {
        input.send(Work((0L..100).random(), (0L..10).random()))
    }
    input.close()
}

suspend fun receiveLotsOfResults(output: Channel<Work>, complete: Channel<Boolean>) {
    repeat(totalWork) {
        val work = output.receive()
        println("${work.x} * ${work.y} = ${work.z}")
    }

    /*output.consumeEach {
        println("${it.x} * ${it.y} = ${it.z}")
    }*/

    complete.send(true)
}