package com.kasiarakos.example7.channels

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce

fun main() = runBlocking {
   /*val channel = produceNumbers()

    for (y in channel){
         println("Receive: $y")
     }

    channel.consumeEach {
        println(it)
    }*/


    /*val producer = produceNumbersPipeline()
    val square = squareNumbersPipeline(producer)
    for (i in 1..5){
        println("received: ${square.receive()}")
    }
    producer.cancel()
    square.cancel()*/


   /* val producer = produceInfinitely()
    repeat(5) {consumer(it ,producer)}
    println("launched")
    delay(950)
    producer.cancel()*/
    val channel  = Channel<String>()
    launch { sendString(channel, "kas", 200L) }
    launch { sendString(channel, "yo", 400L) }

    repeat(6){
       println(channel.receive())
    }

    coroutineContext.cancelChildren()
}

fun CoroutineScope.produceNumbers() = produce<Int> {
    for (x in 1..5) {
        println("send $x")
        send(x)
    }
    close()
}

fun produceNumbers(): Channel<Int> {
    val channel = Channel<Int>()
    GlobalScope.launch {
        for (x in 1..5) {
            println("send $x")
            channel.send(x)
        }
        channel.close()
    }
    return channel;
}

fun CoroutineScope.produceNumbersPipeline() = produce {
    var x = 0
    while (true){
        x++
        send(x)
    }
}

fun CoroutineScope.squareNumbersPipeline(numbers: ReceiveChannel<Int>) = produce {
    numbers.consumeEach { send(it*it) }
}

fun CoroutineScope.produceInfinitely() = produce {
    var x = 1
    while (true){
        send(x++)
        delay(100)
    }
}

fun CoroutineScope.consumer(id: Int, channel: ReceiveChannel<Int>) = GlobalScope.launch {
    channel.consumeEach {
        println("Processor #$id received $it in thread ${Thread.currentThread().name}")
    }
}

suspend fun CoroutineScope.sendString(channel: Channel<String> ,  str: String, delayMs: Long) {
    while (true){
        delay(delayMs)
        channel.send(str)
    }
}