package com.kasiarakos.example2.thread.coroutine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread


fun main(){
    //threadsHelloWorld()
    //coRoutinesHelloWorld()
    threadsCounter(1_000_000)
    //coRoutinesCounter(1_000_000)


    Thread.sleep(10000)
}

fun threadsHelloWorld() {
    thread {
        Thread.sleep(3000)
        println(Thread.currentThread().name)
        println("World")
    }

    println(Thread.currentThread().name)
    println("Hello")
}

fun coRoutinesHelloWorld() {

    GlobalScope.launch {
        delay(3000) //pauses the coroutine does not block the thread
        println(Thread.currentThread().name)
        println("World")
    }

    println(Thread.currentThread().name)
    println("Hello")
}

fun threadsCounter(end: Int){
    val result = AtomicInteger()
    for(i in 1..end){
        thread(start = true){
            //println(Thread.currentThread().name)
            result.getAndIncrement()
        }
    }

    println(result.get())
}

fun coRoutinesCounter(end: Int){
    val result = AtomicInteger()
    for(i in 1..end){
        GlobalScope.launch {
            //println(Thread.currentThread().name)
            result.getAndIncrement()
        }
    }

    println(result.get())
}