package com.kasiarakos.example1.sequence.thread.coroutine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.util.concurrent.ForkJoinPool
import java.util.concurrent.RecursiveTask
import kotlin.system.measureTimeMillis

object App {

    private const val SEQUENTIAL_THRESHOLD = 5000

    fun compute(array: IntArray, low: Int, high: Int) : Long {
        //println("low: $low, high: $high  on ${Thread.currentThread().name}")
        return if(high - low <= SEQUENTIAL_THRESHOLD){
            (low until high)
                .map {
                    array[it].computeLong() }
                .sum()
        }else {
            val mid = low + (high - low) / 2
            val left = compute(array, low, mid)
            val right = compute(array, mid, high)
            return  left + right
        }
    }
}

object AppForkJoin {

    const val SEQUENTIAL_THRESHOLD = 10

    private class Sum(private var array: IntArray, private var low: Int, private var high: Int) : RecursiveTask<Long>() {

        override fun compute(): Long {
            //println("low: $low, high: $high  on ${Thread.currentThread().name}")
            return if (high - low <= SEQUENTIAL_THRESHOLD) {
                (low until high)
                    .map { array[it].computeLong() }
                    .sum()
            } else {
                val mid = low + (high - low) / 2
                val left = Sum(array, low, mid)
                val right = Sum(array, mid, high)
                left.fork()
                val rightAns = right.compute()
                val leftAns = left.join()
                leftAns + rightAns
            }
        }

        fun sumArray(): Long {
            return ForkJoinPool.commonPool().invoke(
                Sum(
                    this.array,
                    0,
                    array.size
                )
            )
        }
    }

    fun computeParallel(array: IntArray, low: Int, high: Int) : Long {
        return Sum(array, low, high).sumArray()
    }

}

object AppCoRoutines {
    private const val SEQUENTIAL_THRESHOLD = 10

    suspend fun compute(array: IntArray, low: Int, high: Int): Long {

//    println("low: $low, high: $high  on ${Thread.currentThread().name}")

        return if (high - low <= SEQUENTIAL_THRESHOLD) {
            (low until high)
                .map { array[it].computeLong() }
                .sum()
        } else {
            val mid = low + (high - low) / 2
            val left = GlobalScope.async {
                compute(
                    array,
                    low,
                    mid
                )
            }
            val right = compute(array, mid, high)
            return left.await() + right
        }
    }
}

fun main() {
    val list = (0..100).toList()
    var result = 0L
    var resultForkJoin = 0L
    var resultCoroutine = 0L

    val time = measureTimeMillis {
        result = App.compute(list.toIntArray(), 0, list.size)
    }

    val timeForkJoin = measureTimeMillis {
        resultForkJoin = AppForkJoin.computeParallel(
            list.toIntArray(),
            0,
            list.size
        )
    }

    var timeCoroutine = 0L
    runBlocking {
        timeCoroutine = measureTimeMillis {
            resultCoroutine =
                AppCoRoutines.compute(list.toIntArray(), 0, list.size)
        }
    }

    println("$result in $time ms sequential")
    println("$resultForkJoin in $timeForkJoin ms forkJoin")
    println("$resultCoroutine in $timeCoroutine ms coroutine")
}

fun Int.computeLong() : Long {
    Thread.sleep(50)
    return this.toLong()
}