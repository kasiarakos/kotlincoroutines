package com.kasiarakos.example4.wait.join.cancel

import kotlinx.coroutines.*
import java.util.concurrent.TimeoutException

fun main() {
//    runWithDelay()
//    println()
//    runWithJoin()

//    longRunningJob()
    longRunningTimeOut()
}

suspend fun runWithDelay() {
    GlobalScope.launch {
        delay(1000)
        println("World")
    }
    println("Hello")
    delay(1500)
}

suspend fun runWithJoin() {
    val job: Job = GlobalScope.launch {
        delay(1000)
        println("World")
    }

    println("Hello")
    job.join()
}

fun longRunningJob() = runBlocking {
    val job = longRunningYieldException()
    delay(100)
    job.cancelAndJoin()

    println("Done")
}

suspend fun longRunningYield(): Job {
    return GlobalScope.launch {
        for (i in 0..20000000) {
            println(i)
            yield()
        }
    }
}

suspend fun longRunningIsActive(): Job {
    return GlobalScope.launch {
        for (i in 0..20000000) {
            if (!this.isActive) return@launch
            println(i)
            yield()
        }
    }
}

suspend fun longRunningYieldException(): Job {
    return GlobalScope.launch {

        try {
            for (i in 0..20000000) {
                println(i)
                yield()
            }
        } catch (ex: CancellationException) {
            println("Message: ${ex.message}")
        } finally {
            withContext(NonCancellable) {
                println("Finally")
            }
        }
    }
}

fun longRunningTimeOut() = runBlocking {
    try {
        withTimeout(1000) {
            for (i in 0..20000000) {
                yield()
                println(i)
            }
        }
    } catch (ex: TimeoutCancellationException) {
        println(ex.message)
    }
}