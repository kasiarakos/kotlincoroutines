package com.kasiarakos.example9.actors

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.atomic.AtomicInteger
import kotlin.coroutines.CoroutineContext
import kotlin.system.measureTimeMillis

open class Counter {
    private var counter = 0

    open suspend fun increment() {
        counter++
    }

    open var value: Int
        get() = counter
        set(value) {
            counter = value
        }

    suspend fun run(context: CoroutineContext, jobs: Int, count: Int, action: suspend () -> Unit): Long {

        return measureTimeMillis {
            val jobs = List(jobs) {
                GlobalScope.launch(context) {
                    repeat(count) { action() }
                }
            }
            jobs.forEach { it.join() }
        }
    }
}

class AtomicCounter : Counter() {
    var counter = AtomicInteger()

    override suspend fun increment() {
        counter.incrementAndGet()
    }

    override var value: Int
        get() = counter.get()
        set(value) = counter.set(value)
}

class MutexCounter : Counter() {
    var counter :Int = 0
    var mutex = Mutex()

    override suspend fun increment() {
        mutex.withLock {
            counter++
        }
    }

    override var value: Int
        get() = counter
        set(value) {
            counter = value
        }
}

sealed class CounterMsg
object InitCounter : CounterMsg()
object IncrementCounter : CounterMsg()
class GetCounter(val response: CompletableDeferred<Int>): CounterMsg()

fun CoroutineScope.counterActor() = actor<CounterMsg> {
    var counter = 0 // actor state
    for (msg in channel) { // iterate over incoming messages
        when (msg) {
            is InitCounter -> counter = 0
            is IncrementCounter -> counter++
            is GetCounter -> msg.response.complete(counter)
        }
    }
}

suspend fun run(context: CoroutineContext, jobs: Int, count: Int, action: suspend () -> Unit): Long {

    return measureTimeMillis {
        val jobs = List(jobs) {
            GlobalScope.launch(context) {
                repeat(count) { action() }
            }
        }
        jobs.forEach { it.join() }
    }
}
/*
fun main() = runBlocking {
    val jobs = 1000
    val count = 1000

    var counter: Counter = Counter()
    var time = counter.run(Dispatchers.Default, jobs, count) {
        counter.increment()
    }
    logResult("Base Counter", jobs, count, time, counter)


    counter.value = 0
    val ctx = newSingleThreadContext("Single Thread Counter")
    time = counter.run(Dispatchers.Default, jobs, count) {
        withContext(ctx) {
            counter.increment()
        }
    }
    logResult("Fine grained", jobs, count, time, counter)

    counter.value = 0
    time = counter.run(ctx, jobs, count) {
        counter.increment()
    }
    logResult("Fine grained", jobs, count, time, counter)


    counter = AtomicCounter()
    time = counter.run(Dispatchers.Default, jobs, count) {
        counter.increment()
    }
    logResult("Atomic", jobs, count, time, counter)


    counter = MutexCounter()
    time = counter.run(Dispatchers.Default, jobs, count) {
        counter.increment()
    }
    logResult("Mutex", jobs, count, time, counter)

    val counterActor = counterActor()
    time = run(Dispatchers.Default, jobs, count) {
        counterActor.send(IncrementCounter)
    }
    logResult("Mutex", jobs, count, time, counter)
    println("Actor completed ${jobs * count} actions in $time ms")
    val future = CompletableDeferred<Int>()
    counterActor.send(GetCounter(future))
    counterActor.close()
    println("Counter    : ${future.await()}")


}*/

private fun logResult(counterType: String, n: Int, k: Int, time: Long, c: Counter) {
    println("${counterType} completed ${n * k} actions in $time ms")
    println("Counter    : ${c.value}")
}