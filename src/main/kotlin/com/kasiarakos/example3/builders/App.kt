package com.kasiarakos.example3.builders

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


fun main() {
    println("Hello")
    GlobalScope.launch {
        delay(500)
        println("World")
    }

    //runBlocking blocks the thread until the coroutine is finished
    runBlocking {
        doWork()
    }


    println("Beautiful")
    Thread.sleep(3000)
}

suspend fun doWork() {
    delay(600)
}
