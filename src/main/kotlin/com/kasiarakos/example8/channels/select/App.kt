package com.kasiarakos.example8.channels.select

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.selects.select

fun CoroutineScope.producer1() = produce {
    while (true) {
        //delay(200)
        send("Producer 1")
    }
}

fun CoroutineScope.producer2() = produce {
    while (true) {
        //delay(300)
        send("Producer 2")
    }
}

fun CoroutineScope.producer1Send() = produce {
     send("Producer 1")
}

fun CoroutineScope.producer2Send() = produce {
    send("Producer 2")
}

suspend fun selector(message1: ReceiveChannel<String>, message2: ReceiveChannel<String>) {
    select<Unit> {
        message1.onReceive { println(it) }
        message2.onReceive { println(it) }
    }
}

@UseExperimental(InternalCoroutinesApi::class)
suspend fun selectorString(message1: ReceiveChannel<String>, message2: ReceiveChannel<String>) : String =
    select<String> {
        message1.onReceiveOrNull{it ?: "Channel 1 is Closed" }
        message2.onReceiveOrNull{it ?: "Channel 2 is Closed" }

    }

fun CoroutineScope.produceNumbers(sideChannel: SendChannel<Int>) = produce<Int> {
    for(i in 1..10){
        delay(100)
        select<Unit> {
            onSend(i){}
            sideChannel.onSend(i){}
        }
    }
    println("Done sending")
}

fun main() = runBlocking {
   /* val m1 = producer1()
    val m2 = producer2()

    repeat(15) {
        selector(m1, m2)
    }*/

    /*val m1 = producer1Send()
    val m2 = producer2Send()

    repeat(15) {
        println(selectorString(m1, m2))
    }*/

    val side = Channel<Int>()

    val producer = produceNumbers(side)
    GlobalScope.launch {

        side.consumeEach {
            println("Side $it")
        }
    }

    producer.consumeEach {
        println(it)
        delay(500)
    }
}