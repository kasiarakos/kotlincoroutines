package com.kasiarakos.example5.coroutine.context

import kotlinx.coroutines.*


fun main() = runBlocking {
   /* val jobs = mutableListOf<Job>()

    createJobs(jobs)

    jobs.forEach { it.join() }

    println("\n\n")

    var job = launch(Dispatchers.Default) {
        println("Launched thread: in thread ${Thread.currentThread().name}")
        val jobs = mutableListOf<Job>()
        createJobs(jobs)
        jobs.forEach { it.join() }
    }*/

    //accessJobObj()
    //playWithParentChildCoRoutines()
    playWithSingleThread()
}

private fun CoroutineScope.createJobs(jobs: MutableList<Job>) {

    jobs += launch {
        println("            default: In thread ${Thread.currentThread().name}")
    }

    jobs += launch(Dispatchers.Default) {
        println("  defaultDispatcher: In thread ${Thread.currentThread().name}")
    }

    jobs += launch(Dispatchers.Unconfined) {
        println("         Unconfined: In thread ${Thread.currentThread().name}")
    }

    jobs += launch(coroutineContext) {
        println("   coroutineContext: In thread ${Thread.currentThread().name}")
    }

    jobs += launch(Dispatchers.Default) {
        println("         CommonPool: In thread ${Thread.currentThread().name}")
    }

    jobs += launch(newSingleThreadContext("myThread")) {
        println("SingleThreadContext: In thread ${Thread.currentThread().name}")
    }
}

private fun accessJobObj() = runBlocking {
    val job = launch {
        println("isActive?  ${coroutineContext.isActive}")
    }
    job.join()

}

private fun playWithParentChildCoRoutines() = runBlocking {
    val outer = launch {
        launch {
            repeat(1000){
                print('.')
                delay(1)
            }
        }
    }

    delay(200)
    outer.cancelChildren()
    delay(1000)
    println("\n\nFinished")

}

fun playWithSingleThread() = runBlocking {
    newSingleThreadContext("SingleThreadContext").use {
        val job = launch(it) {
            println("I am running in ${Thread.currentThread().name}")
        }
        job.join()
    }
}
