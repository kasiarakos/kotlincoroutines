package com.kasiarakos.example6.async.await

import kotlinx.coroutines.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun main() = runBlocking {

   /* val job = launch {

        val time = measureTimeMillis {
            val r1 = async { doWorkOne() }
            val r2 = async { doWorkTwo() }
            println("Result: ${r1.await() + r2.await()}")
        }
        println("Done: $time")

    }
    job.join()*/

   /* val result = doWorkAsync("yo")
    println(result.await())*/

    val job = GlobalScope.launch {
        val result = GlobalScope.async(start = CoroutineStart.LAZY) { doWorkLazy("yo") }
        println(result.await())
    }

    job.join()
}

suspend fun doWorkOne(): Int {
    delay(100)
    println("Work-1")
    return Random(System.currentTimeMillis()).nextInt(42)
}

suspend fun doWorkTwo(): Int {
    delay(200)
    println("Work-2")
    return Random(System.currentTimeMillis()).nextInt(42)
}

fun doWorkAsync(msg: String) : Deferred<Int> = GlobalScope.async {
    log("$msg Working")
    delay(5000)
    log("$msg Work Done")
    return@async 33
}

fun log(str: String){
    println("$str in ${Thread.currentThread().name}")
}

suspend fun doWorkLazy(msg: String) : Int{
    log("$msg Working")
    delay(2000)
    log("$msg Work Done")
    return 33
}